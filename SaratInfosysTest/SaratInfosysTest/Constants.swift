//
//  Constants.swift
//  SaratInfosysTest
//
//  Created by Good Morning on 17/04/20.
//  Copyright © 2020 Pune. All rights reserved.
//

import Foundation

let REQUEST_URL_STRING = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"

let DEFAULT_RESPONSE_SCREEN_TITLE = "Loading Data..."
let PLEASE_WAIT = "Please wait, Refreshing data!"
let PULL_DOWN_TO_REFRESH = "Pull down to refresh"
let EMPTY_TITLE = "<No Name>"
let EMPTY_DESCRIPTION = "<No Description>"
let EMPTY_STRING = ""
let HOME_SCREEN_TITLE = "Home"
let REQUEST_BUTTON_TITLE = "Click to see result"

let OK = "Ok"
let WARNING = "Warning!"
let SUCCESS = "Success"
let FAILED = "Download Failed, Please try again!"
let NETWORK_LOSS = "Unable to connect. Please check your network connection"

