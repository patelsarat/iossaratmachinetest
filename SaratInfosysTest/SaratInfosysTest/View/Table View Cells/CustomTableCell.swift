//
//  CustomTableCell.swift
//  SaratInfosysTest
//
//  Created by Good Morning on 16/04/20.
//  Copyright © 2020 Pune. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    let itemImageView = UIImageView()
    let labelTitle = UILabel()
    let labelDesctiption = UILabel()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //Do your cell set up
        
        itemImageView.image = UIImage(named: "placeholderInfoAssignment")
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        
        labelTitle.translatesAutoresizingMaskIntoConstraints = false
        
        labelDesctiption.translatesAutoresizingMaskIntoConstraints = false
        labelDesctiption.numberOfLines = 0
        
        contentView.addSubview(itemImageView)
        contentView.addSubview(labelTitle)
        contentView.addSubview(labelDesctiption)
        
        let viewsDict = [
            "image": itemImageView,
            "username": labelTitle,
            "message": labelDesctiption,
        ]
        
        //To set layout here we are going to use VFL and NSLayout
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        itemImageView.centerYAnchor.constraint(equalTo:self.centerYAnchor).isActive = true
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=5)-[image(70)]-(>=5)-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[username]-[message(>=40)]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[image(70)]-[username]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[image(70)]-[message]-|", options: [], metrics: nil, views: viewsDict))
    }
}

