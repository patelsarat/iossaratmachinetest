//
//  HomeViewController.swift
//  SaratInfosysTest
//
//  Created by Good Morning on 16/04/20.
//  Copyright © 2020 Pune. All rights reserved.
//

import UIKit

class HomeViewController : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCustomItems()
    }
    
    func setupCustomItems() {
        self.view.backgroundColor = .groupTableViewBackground
        self.title = HOME_SCREEN_TITLE
        
        let button = UIButton(type: .system)
        button.setTitle(REQUEST_BUTTON_TITLE, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)

        button.translatesAutoresizingMaskIntoConstraints = false
        button.centerYAnchor.constraint(equalTo:self.view.centerYAnchor).isActive = true
        button.centerXAnchor.constraint(equalTo:self.view.centerXAnchor).isActive = true
    }
    
    @objc func buttonAction(sender: UIButton!) {
        let secondViewController = DataViewController()
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
}
