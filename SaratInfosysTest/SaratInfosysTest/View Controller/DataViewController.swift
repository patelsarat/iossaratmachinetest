//
//  DataViewController.swift
//  SaratInfosysTest
//
//  Created by Good Morning on 16/04/20.
//  Copyright © 2020 Pune. All rights reserved.
//

import UIKit
import SDWebImage

class DataViewController : UIViewController {
    var refreshControl = UIRefreshControl()
    let tableView = UITableView()
    var dataModel = DataViewModel()
    
    //View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCustomItems()
        self.getData()
    }
    
    //local methods
    func setupCustomItems() {
        self.view.backgroundColor = .groupTableViewBackground
        self.title = DEFAULT_RESPONSE_SCREEN_TITLE

        //Table View Properties
        tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
        
        refreshControl.attributedTitle = NSAttributedString(string: PLEASE_WAIT)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        var activityIndicatorView: UIActivityIndicatorView!
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()

        //Autolayout handling
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func getData() {
        self.dataModel.downloadData(completionHanlder: { (response) in
            DispatchQueue.main.async {
                if response == SUCCESS {
                    self.title = self.dataModel.title ?? self.title
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
                else {
                    self.title = PULL_DOWN_TO_REFRESH
                    self.refreshControl.endRefreshing()
                    let alert = UIAlertController(title: WARNING, message: response, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: OK, style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        })
    }

    @objc func refresh() {
        self.getData()
    }
}

extension DataViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataModel.rows != nil ? (self.dataModel.rows?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CustomTableViewCell(style: .subtitle, reuseIdentifier: "Cell Identier")
        cell.selectionStyle = .none
        let item = self.dataModel.rows?[indexPath.row]
        cell.labelTitle.text = item?.title ?? EMPTY_TITLE
        cell.labelDesctiption.text = item?.description ?? EMPTY_DESCRIPTION
        let imageURL = item?.imageHref ?? EMPTY_STRING
        cell.itemImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "placeholderInfoAssignment.png"))
        return cell
    }

    //Method required to have dynamic height for table view cell
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

/* We can use this URLSession extension to download images in background insted SDWebImage library.
extension UIImageView {
    func setImageFromUrl(ImageURL :String) {
        URLSession.shared.dataTask( with: NSURL(string:ImageURL)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                if let data = data {
                    self.image = UIImage(data: data)
                }
            }
        }).resume()
    }
}
*/
