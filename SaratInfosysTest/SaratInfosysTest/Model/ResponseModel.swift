//
//  DataResponse.swift
//  SaratInfosysTest
//
//  Created by Good Morning on 17/04/20.
//  Copyright © 2020 Pune. All rights reserved.
//

struct ResponseData : Decodable {
    var title : String
    var rows : [ItemInfo]
}

struct ItemInfo : Decodable {
    var title : String? //Optional variables as it can be null in response
    var description : String? //Optional variables as it can be null in response
    var imageHref : String? //Optional variables as it can be null in response
}
