//
//  DataViewModel.swift
//  SaratInfosysTest
//
//  Created by Good Morning on 17/04/20.
//  Copyright © 2020 Pune. All rights reserved.
//

import Foundation
import UIKit

class DataViewModel {
    private var responseData: ResponseData?

    public var title: String? {
        return responseData?.title
    }
    
    public var rows: [ItemInfo]? {
        return responseData?.rows
    }
    
    /* downloadData will be used to download required data from mentioned URL
     * URLSession used as per guidline
     * Insted to use 3rd party library, JSONDecoder() used. */
    func downloadData(completionHanlder: @escaping (_ response: String ) -> Void) {
        
        guard let urlObj = URL(string: REQUEST_URL_STRING) else { return }
        URLSession.shared.dataTask(with: urlObj) { (data, response, error) in
            do {
                if let error = error {
                    completionHanlder(error.localizedDescription.count > 0 ? error.localizedDescription : NETWORK_LOSS )
                    return
                }
                guard let data = data else { return }
                guard let utf8Data = String(decoding: data, as: UTF8.self).data(using: .utf8) else { return }
                self.responseData = try JSONDecoder().decode(ResponseData.self, from: utf8Data)
                completionHanlder(SUCCESS)
            } catch {
                completionHanlder(FAILED)
            }
            }.resume()
    }
}
